package events.vandal.summerparkour.events

import com.destroystokyo.paper.event.server.ServerTickEndEvent
import com.mojang.authlib.GameProfile
import events.vandal.summerparkour.SummerParkour
import events.vandal.summerparkour.parkour.ParkourManager
import events.vandal.summerparkour.parkour.ParkourPlayer
import events.vandal.summerparkour.util.TimeFormatter
import net.kyori.adventure.text.Component
import net.kyori.adventure.text.format.NamedTextColor
import org.bukkit.ChatColor
import org.bukkit.Sound
import org.bukkit.SoundCategory
import org.bukkit.block.Block
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.event.player.PlayerMoveEvent
import org.bukkit.event.player.PlayerQuitEvent

object ParkourEvent : Listener {
    private val lastPlayerInteraction = mutableMapOf<Player, Block>()

    @EventHandler
    fun onWorldTick(ev: ServerTickEndEvent) {
        ParkourManager.players.toMutableMap().forEach {
            val player = SummerParkour.plugin.server.getPlayer(it.key)

            if (player == null) {
                ParkourManager.players.remove(it.key)

                return@forEach
            }

            val time = System.currentTimeMillis() - it.value.startTime

            if (time >= 50_000_000) {
                player.inventory.contents = it.value.originalInventory?.toTypedArray()

                ParkourManager.players.remove(it.key)
                player.sendMessage("${ChatColor.RED}${ChatColor.BOLD}Parkour failed! ${ChatColor.RESET}${ChatColor.RED}You took too long!")

                return@forEach
            }

            player.sendActionBar(Component.text(TimeFormatter.formatString(time)).color(NamedTextColor.GREEN))
        }
    }

    @EventHandler
    fun onPlayerMove(ev: PlayerMoveEvent) {
        // This is to ensure the pressure plates don't end up getting spammed.
        // There's probably a better way of doing this, but I'm tired of Bukkit.
        if (lastPlayerInteraction.contains(ev.player)) {
            if (ev.player.location.block != lastPlayerInteraction[ev.player]) {
                lastPlayerInteraction.remove(ev.player)
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    fun onPressurePlateInteract(ev: PlayerInteractEvent) {
        if (ev.hasBlock() && ev.action == Action.PHYSICAL) {
            if (ev.clickedBlock == null)
                return

            if (lastPlayerInteraction.contains(ev.player) && lastPlayerInteraction[ev.player] == ev.clickedBlock) // Ensure the player doesn't accidentally spam.
                return

            // Start parkour
            if (ParkourManager.parkourMap.any { it.value.startPosition?.location?.block == ev.clickedBlock!! }) {
                ev.isCancelled = true

                lastPlayerInteraction[ev.player] = ev.clickedBlock!!

                if (SummerParkour.selectedParkours[ev.player] != null) {
                    ev.player.sendMessage("${ChatColor.YELLOW}SummerParkour >> ${ChatColor.RED}You currently still have a parkour selected, please deselect it before doing parkour!")
                    return
                }

                val parkours = ParkourManager.parkourMap.filter { it.value.startPosition?.location?.block == ev.clickedBlock!! }
                val parkour = parkours[parkours.keys.first()]!!

                val parkourPlayer = ParkourPlayer(
                    System.currentTimeMillis(),
                    mutableListOf(),
                    parkour.uuid,
                    // Store the player's original inventory.
                    if (ParkourManager.players.contains(ev.player.uniqueId) && ParkourManager.players[ev.player.uniqueId]!!.currentParkour == parkour.uuid)
                        ParkourManager.players[ev.player.uniqueId]!!.originalInventory
                    else
                        ev.player.inventory.contents?.toList()
                )

                if (ParkourManager.players.contains(ev.player.uniqueId) && ParkourManager.players[ev.player.uniqueId]!!.currentParkour == parkour.uuid) {
                    ev.player.sendMessage("${ChatColor.GREEN}Reset the timer to ${ChatColor.YELLOW}00:00:00.000, ${ChatColor.GREEN}get to the finish line!")
                } else {
                    ev.player.sendMessage("${ChatColor.GREEN}Started the timer, get to the finish line!")

                    ev.player.inventory.contents = ParkourManager.customInventory.contents
                }

                val metrics = SummerParkour.metrics.getMetric(GameProfile(ev.player.playerProfile.id, ev.player.playerProfile.name))

                metrics.totalAttempts[parkour.uuid] = (metrics.totalAttempts[parkour.uuid] ?: 0) + 1

                ev.player.playSound(ev.player.location, Sound.BLOCK_METAL_PRESSURE_PLATE_CLICK_ON, SoundCategory.BLOCKS, .4F, 1F)

                ParkourManager.players[ev.player.uniqueId] = parkourPlayer

                // End parkour
            } else if (ParkourManager.parkourMap.any { it.value.endPosition?.location?.block == ev.clickedBlock!! }) {
                ev.isCancelled = true

                lastPlayerInteraction[ev.player] = ev.clickedBlock!!

                val parkours = ParkourManager.parkourMap.filter { it.value.endPosition?.location?.block == ev.clickedBlock!! }
                val parkour = parkours[parkours.keys.first()]!!

                // Tell the player that they haven't even started it yet.
                if (!ParkourManager.players.contains(ev.player.uniqueId)) {
                    ev.player.sendMessage("${ChatColor.YELLOW}This is the finishing point of the parkour! Get to the start, and see how you fare!")

                    return
                }

                val parkourPlayer = ParkourManager.players[ev.player.uniqueId]!!

                if (parkour.checkpoints.size > parkourPlayer.collectedCheckpoints.size) {
                    ev.player.sendMessage("${ChatColor.RED}You haven't collected all of the checkpoints yet, go back and collect them before finishing!")
                    ev.player.playSound(ev.player.location, Sound.ENTITY_ENDERMAN_TELEPORT, SoundCategory.BLOCKS, .4F, .2F)
                } else {
                    val finishTime = System.currentTimeMillis() - parkourPlayer.startTime

                    ev.player.sendMessage("${ChatColor.GREEN}${ChatColor.BOLD}Finished the parkour! ${ChatColor.RESET}${ChatColor.GREEN}Your time: ${ChatColor.YELLOW}${TimeFormatter.formatString(finishTime)}")
                    ev.player.playSound(ev.player.location, Sound.ENTITY_EXPERIENCE_ORB_PICKUP, SoundCategory.BLOCKS, .4F, 1F)

                    val metrics = SummerParkour.metrics.getMetric(GameProfile(ev.player.playerProfile.id, ev.player.playerProfile.name))
                    metrics.addParkourPlayer(parkourPlayer, finishTime)

                    SummerParkour.metrics.save()

                    ev.player.inventory.contents = parkourPlayer.originalInventory?.toTypedArray()

                    ParkourManager.players.remove(ev.player.uniqueId)
                }
                // Checkpoint
            } else if (ParkourManager.parkourMap.any { it.value.checkpoints.any { checkpoint -> checkpoint.location.block == ev.clickedBlock!! } }) {
                ev.isCancelled = true

                lastPlayerInteraction[ev.player] = ev.clickedBlock!!

                val parkourPlayer = ParkourManager.players[ev.player.uniqueId] ?: return
                val parkour = ParkourManager.parkourMap[parkourPlayer.currentParkour] ?: return

                val checkpoint = parkour.checkpoints.firstOrNull { it.location.block == ev.clickedBlock!! } ?: return

                if (parkourPlayer.collectedCheckpoints.any { it.second.block == checkpoint.location.block })
                    return

                val checkpointIndex = parkour.checkpoints.indexOf(checkpoint)

                if (checkpointIndex > 0 && parkourPlayer.collectedCheckpoints.none { it.second.block == parkour.checkpoints[checkpointIndex - 1].location.block }) {
                    ev.player.sendMessage("${ChatColor.RED}${ChatColor.BOLD}You haven't collected ${ChatColor.RESET}${ChatColor.YELLOW}Checkpoint #$checkpointIndex ${ChatColor.RED}${ChatColor.BOLD}yet! ${ChatColor.RESET}${ChatColor.RED}You should go back and collect it, otherwise you will be unable to complete the course!")

                    return
                }

                val time = System.currentTimeMillis()

                val totalTime = time - (if (checkpointIndex == 0) parkourPlayer.startTime else parkourPlayer.collectedCheckpoints.last().first)

                ev.player.sendMessage("${ChatColor.GREEN}Reached ${ChatColor.YELLOW}Checkpoint #${checkpointIndex + 1} ${ChatColor.GREEN}in ${ChatColor.GOLD}${TimeFormatter.formatString(totalTime)}")
                ev.player.playSound(ev.player.location, Sound.BLOCK_METAL_PRESSURE_PLATE_CLICK_ON, SoundCategory.BLOCKS, .4F, 1F)
                parkourPlayer.collectedCheckpoints.add(Pair(time, checkpoint.location))
            }
        } else if (ev.action == Action.LEFT_CLICK_BLOCK) {
            if (ParkourManager.parkourMap.any { it.value.startPosition?.location?.block == ev.clickedBlock || it.value.endPosition?.location?.block == ev.clickedBlock || it.value.checkpoints.any { checkpoint -> checkpoint.location.block == ev.clickedBlock } }) {
                ev.isCancelled = true
                return
            }
        }
    }

    @EventHandler
    fun onPlayerDisconnect(ev: PlayerQuitEvent) {
        if (ParkourManager.players.contains(ev.player.uniqueId)) {
            ev.player.inventory.contents = ParkourManager.players[ev.player.uniqueId]!!.originalInventory?.toTypedArray()

            ParkourManager.players.remove(ev.player.uniqueId)
        }
    }
}