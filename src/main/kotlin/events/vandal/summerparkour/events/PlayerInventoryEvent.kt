package events.vandal.summerparkour.events

import events.vandal.summerparkour.parkour.ParkourManager
import net.kyori.adventure.text.Component
import net.kyori.adventure.text.format.NamedTextColor
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.entity.EntityType
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.entity.EntityPickupItemEvent
import org.bukkit.event.inventory.InventoryClickEvent
import org.bukkit.event.inventory.InventoryCloseEvent
import org.bukkit.event.inventory.InventoryInteractEvent
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.inventory.Inventory
import org.bukkit.inventory.ItemStack

object PlayerInventoryEvent : Listener {
    val customInventories = mutableMapOf<Player, Inventory>()

    @EventHandler
    fun onPlayerInteractWithInventory(ev: InventoryInteractEvent) {
        // Prevents interactions with the Parkour custom inventory.
        if (ParkourManager.players.contains(ev.whoClicked.uniqueId)) {
            ev.isCancelled = true
        }
    }

    @EventHandler
    fun onPlayerClickInventory(ev: InventoryClickEvent) {
        if (customInventories.contains(ev.whoClicked)) {
            val inventory = customInventories[ev.whoClicked]!!

            if (ev.clickedInventory != inventory)
                return

            val parkourPlayer = ParkourManager.players[ev.whoClicked.uniqueId]!!
            val parkour = ParkourManager.parkourMap[parkourPlayer.currentParkour]!!

            if (ev.currentItem != null) {
                if (ev.currentItem!!.type == Material.GOLD_BLOCK) {
                    ev.whoClicked.teleport(parkour.startPosition!!.location.clone().add(0.0, 1.0, 0.0))

                    ev.inventory.close()
                } else if (ev.currentItem!!.type == Material.IRON_BLOCK) {
                    ev.whoClicked.teleport(parkourPlayer.collectedCheckpoints[ev.currentItem!!.itemMeta.customModelData].second.clone().add(0.0, 0.5, 0.0))
                    ev.inventory.close()
                }

                ev.isCancelled = true
            }
        }
    }

    @EventHandler
    fun onPlayerCloseInventory(ev: InventoryCloseEvent) {
        if (customInventories.contains(ev.player)) {
            customInventories.remove(ev.player)
        }
    }

    @EventHandler
    fun onPlayerPickUpItem(ev: EntityPickupItemEvent) {
        if (ev.entity.type != EntityType.PLAYER)
            return

        val player = ev.entity as Player
        if (ParkourManager.players.contains(player.uniqueId))
            ev.isCancelled = true
    }

    @EventHandler
    fun onPlayerClick(ev: PlayerInteractEvent) {
        if (ParkourManager.players.contains(ev.player.uniqueId)) {
            val parkourPlayer = ParkourManager.players[ev.player.uniqueId]!!
            val parkour = ParkourManager.parkourMap[parkourPlayer.currentParkour]!!

            if (ev.item == ParkourManager.customInventory.getItem(3)) { // Teleport to Checkpoint
                if (ev.action == Action.LEFT_CLICK_AIR || ev.action == Action.LEFT_CLICK_BLOCK) { // GUI select
                    // Inventory GUI creation
                    val inventory = Bukkit.createInventory(null, 27).apply {
                        setItem(0, ItemStack(Material.GOLD_BLOCK).apply {
                            val itemMeta = this.itemMeta
                            itemMeta.displayName(Component.text("Start of Parkour").color(NamedTextColor.GOLD))

                            this.itemMeta = itemMeta
                        })

                        parkour.checkpoints.forEachIndexed { index, checkpoint ->
                            if (parkourPlayer.collectedCheckpoints.any { it.second == checkpoint.location }) {
                                addItem(ItemStack(Material.IRON_BLOCK, index + 1).apply {
                                    val itemMeta = this.itemMeta
                                    itemMeta.displayName(Component.text("Checkpoint ${index + 1}").color(NamedTextColor.YELLOW))
                                    itemMeta.setCustomModelData(index)

                                    this.itemMeta = itemMeta
                                })
                            } else {
                                addItem(ItemStack(Material.BARRIER, index + 1).apply {
                                    val itemMeta = this.itemMeta
                                    itemMeta.displayName(Component.text("Checkpoint ${index + 1}").color(NamedTextColor.RED))

                                    this.itemMeta = itemMeta
                                })
                            }
                        }
                    }

                    ev.player.openInventory(inventory)
                    customInventories[ev.player] = inventory
                } else if (ev.action == Action.RIGHT_CLICK_AIR || ev.action == Action.RIGHT_CLICK_BLOCK) { // Quick TP
                    if (parkourPlayer.collectedCheckpoints.isEmpty()) {
                        ev.player.teleport(parkour.startPosition!!.location.clone().add(0.0, 1.0, 0.0))
                    } else {
                        val latestCheckpoint = parkourPlayer.collectedCheckpoints.last().second.clone()
                        ev.player.teleport(latestCheckpoint.add(0.0, 0.5, 0.0))
                    }
                }
            } else if (ev.item == ParkourManager.customInventory.getItem(4)) { // Reset to Start
                if (ev.action != Action.PHYSICAL) { // just make this easier for ourselves ffs.
                    ev.player.teleport(parkour.startPosition!!.location.clone().add(0.0, 1.0, 0.0))
                }
            } else if (ev.item == ParkourManager.customInventory.getItem(5)) { // Cancel
                if (ev.action != Action.PHYSICAL) {
                    ev.player.sendMessage("${ChatColor.RED}Cancelled parkour!")
                    ev.player.inventory.contents = parkourPlayer.originalInventory?.toTypedArray()
                    ParkourManager.players.remove(ev.player.uniqueId)
                }
            }
        }
    }
}