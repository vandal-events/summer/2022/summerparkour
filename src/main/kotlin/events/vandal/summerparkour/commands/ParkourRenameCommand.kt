package events.vandal.summerparkour.commands

import dev.jorel.commandapi.executors.PlayerCommandExecutor
import events.vandal.summerparkour.parkour.ParkourManager
import org.bukkit.ChatColor
import org.bukkit.entity.Player

object ParkourRenameCommand : PlayerCommandExecutor {
    override fun run(sender: Player, args: Array<out Any>) {
        val parkour = ParkourManager.getSelectedParkourOrReport(sender) ?: return

        val newName = args[0] as String

        sender.sendMessage("${ChatColor.GREEN}Renamed ${ChatColor.YELLOW}\"${parkour.name}\" ${ChatColor.GREEN}to ${ChatColor.YELLOW}\"$newName\"${ChatColor.GREEN}.")

        parkour.name = newName
        ParkourManager.save()
    }
}