package events.vandal.summerparkour.commands.select

import dev.jorel.commandapi.executors.PlayerCommandExecutor
import events.vandal.summerparkour.SummerParkour
import org.bukkit.ChatColor
import org.bukkit.entity.Player

object ParkourDeselectCommand : PlayerCommandExecutor {
    override fun run(sender: Player, args: Array<out Any>) {
        SummerParkour.selectedParkours.remove(sender)
        sender.sendMessage("${ChatColor.GREEN}Successfully deselected parkour!")
    }
}