package events.vandal.summerparkour.commands.select

import dev.jorel.commandapi.executors.PlayerCommandExecutor
import events.vandal.summerparkour.SummerParkour
import events.vandal.summerparkour.parkour.ParkourManager
import org.bukkit.ChatColor
import org.bukkit.entity.Player
import java.util.*

object ParkourSelectCommand : PlayerCommandExecutor {
    override fun run(sender: Player, args: Array<out Any>) {
        val uuidString = args[0] as String
        try {
            val uuid = UUID.fromString(uuidString)
            val parkour = ParkourManager.parkourMap[uuid]

            if (parkour == null) {
                sender.sendMessage("${ChatColor.RED}Parkour does not exist!")

                return
            }

            SummerParkour.selectedParkours[sender] = uuid

            sender.sendMessage("${ChatColor.GREEN}Successfully selected parkour ${ChatColor.YELLOW}\"${parkour.name}\"${ChatColor.GREEN}.")
        } catch (e: Exception) {
            sender.sendMessage("${ChatColor.RED}Failed to select parkour!")
            e.printStackTrace()
        }
    }
}