package events.vandal.summerparkour.commands.select

import dev.jorel.commandapi.executors.PlayerCommandExecutor
import events.vandal.summerparkour.SummerParkour
import events.vandal.summerparkour.parkour.ParkourManager
import org.bukkit.ChatColor
import org.bukkit.entity.Player

object ParkourSelectedCommand : PlayerCommandExecutor {
    override fun run(sender: Player, args: Array<out Any>) {
        val selectedParkour = ParkourManager.getSelectedParkourOrReport(sender) ?: return

        sender.sendMessage("${ChatColor.GREEN}You currently have the parkour ${ChatColor.YELLOW}\"${selectedParkour.name}\" ${ChatColor.GREEN}selected. ${ChatColor.DARK_GRAY}(UUID: ${selectedParkour.uuid})")
    }
}