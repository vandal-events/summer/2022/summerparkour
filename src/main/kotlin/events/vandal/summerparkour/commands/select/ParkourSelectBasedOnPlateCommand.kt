package events.vandal.summerparkour.commands.select

import dev.jorel.commandapi.executors.PlayerCommandExecutor
import events.vandal.summerparkour.SummerParkour
import events.vandal.summerparkour.parkour.ParkourManager
import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.entity.Player

object ParkourSelectBasedOnPlateCommand : PlayerCommandExecutor {
    override fun run(sender: Player, args: Array<out Any>) {
        val selected = sender.getTargetBlock(5)

        if (selected == null || selected.type.isAir) {
            sender.sendMessage("${ChatColor.RED}Target block is air!")
            return
        }

        if (selected.type == Material.HEAVY_WEIGHTED_PRESSURE_PLATE || selected.type == Material.LIGHT_WEIGHTED_PRESSURE_PLATE) {
            val parkours = SummerParkour.filterParkours(selected.location)

            if (parkours.isEmpty()) {
                sender.sendMessage("${ChatColor.GREEN}Unable to find parkour!")

                return
            }

            val parkour = parkours.values.first()

            SummerParkour.selectedParkours[sender] = parkour.uuid

            sender.sendMessage("${ChatColor.GREEN}Selected parkour, found to be ${ChatColor.YELLOW}\"${parkour.name}\"")
        } else {
            sender.sendMessage("${ChatColor.RED}Not targeting a checkpoint or start/stop block!")
        }
    }
}