package events.vandal.summerparkour.commands

import dev.jorel.commandapi.executors.PlayerCommandExecutor
import events.vandal.summerparkour.parkour.ParkourManager
import org.bukkit.ChatColor
import org.bukkit.Location
import org.bukkit.entity.Player

object ParkourInfoCommand : PlayerCommandExecutor {
    override fun run(sender: Player, args: Array<out Any>) {
        val parkour = ParkourManager.getSelectedParkourOrReport(sender) ?: return

        sender.sendMessage("${ChatColor.GREEN}Parkour Name : ${ChatColor.YELLOW}${parkour.name}")
        sender.sendMessage("${ChatColor.DARK_GREEN}Parkour UUID : ${ChatColor.YELLOW}${parkour.uuid}")
        sender.sendMessage("${ChatColor.GREEN}Starting Location : ${ChatColor.YELLOW}${locationToText(parkour.startPosition?.location)}")
        sender.sendMessage("${ChatColor.DARK_GREEN}Checkpoints : ${ChatColor.YELLOW}(${parkour.checkpoints.size})")
        var alternating = false
        parkour.checkpoints.forEach {
            sender.sendMessage("${if (alternating) ChatColor.DARK_GREEN else ChatColor.GREEN}- ${ChatColor.AQUA}${locationToText(it.location)}")
            alternating = !alternating
        }

        sender.sendMessage("${if (!alternating) ChatColor.DARK_GREEN else ChatColor.GREEN}Ending Location : ${ChatColor.YELLOW}${locationToText(parkour.endPosition?.location)}")
    }

    private fun locationToText(location: Location?): String {
        if (location == null)
            return "${ChatColor.RED}None (not set)"

        return "X: ${location.blockX}, Y: ${location.blockY}, Z: ${location.blockZ}, World: ${location.world.key}"
    }
}