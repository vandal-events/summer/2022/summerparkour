package events.vandal.summerparkour.commands.checkpoint

import dev.jorel.commandapi.executors.PlayerCommandExecutor
import events.vandal.summerparkour.SummerParkour
import events.vandal.summerparkour.parkour.ParkourCheckpoint
import events.vandal.summerparkour.parkour.ParkourManager
import net.kyori.adventure.text.Component
import net.kyori.adventure.text.format.NamedTextColor
import net.kyori.adventure.text.format.TextDecoration
import org.bukkit.ChatColor
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.entity.ArmorStand
import org.bukkit.entity.EntityType
import org.bukkit.entity.Player
import org.bukkit.event.entity.CreatureSpawnEvent

object ParkourCheckpointAddCommand : PlayerCommandExecutor {
    override fun run(sender: Player, args: Array<out Any>) {
        if (SummerParkour.filterParkours(sender.location).isNotEmpty()) {
            sender.sendMessage("${ChatColor.RED}Position has already been taken by another point!")
            return
        }

        val parkour = ParkourManager.getSelectedParkourOrReport(sender) ?: return

        val checkpoint = addCheckpoint(sender, parkour.checkpoints.size)

        parkour.checkpoints.add(checkpoint)
        ParkourManager.save()

        sender.sendMessage("${ChatColor.GREEN}Successfully created ${ChatColor.YELLOW}Checkpoint #${parkour.checkpoints.size}${ChatColor.GREEN}!")
    }

    fun addCheckpoint(sender: Player, index: Int): ParkourCheckpoint {
        val location = sender.location

        val entity = location.world.spawnEntity(Location(location.world, location.blockX + 0.5, location.blockY + 0.5, location.blockZ + 0.5), EntityType.ARMOR_STAND, CreatureSpawnEvent.SpawnReason.CUSTOM) {
            val armorStand = it as ArmorStand
            armorStand.isSmall = true
            armorStand.isVisible = false
            armorStand.isInvisible = true
            armorStand.isInvulnerable = false
            armorStand.isCustomNameVisible = true
            armorStand.customName(Component.text("Checkpoint ${index + 1}").color(NamedTextColor.WHITE))
            armorStand.setGravity(false)
        }

        location.block.type = Material.HEAVY_WEIGHTED_PRESSURE_PLATE

        return ParkourCheckpoint(
            location,
            entity.uniqueId
        )
    }
}