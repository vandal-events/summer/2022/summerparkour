package events.vandal.summerparkour.commands.checkpoint

import dev.jorel.commandapi.executors.PlayerCommandExecutor
import events.vandal.summerparkour.SummerParkour
import events.vandal.summerparkour.parkour.ParkourManager
import org.bukkit.ChatColor
import org.bukkit.entity.Player

object ParkourCheckpointAddIndexedCommand : PlayerCommandExecutor {
    override fun run(sender: Player, args: Array<out Any>) {
        if (SummerParkour.filterParkours(sender.location).isNotEmpty()) {
            sender.sendMessage("${ChatColor.RED}Position has already been taken by another point!")
            return
        }

        val parkour = ParkourManager.getSelectedParkourOrReport(sender) ?: return

        val index = (args[0] as Int) - 1

        if (index < 0 || index > parkour.checkpoints.size) {
            sender.sendMessage("${ChatColor.RED}Index out of range!")
            return
        }

        val checkpoint = ParkourCheckpointAddCommand.addCheckpoint(sender, index)

        parkour.checkpoints.add(index, checkpoint)
        ParkourManager.save()

        sender.sendMessage("${ChatColor.GREEN}Successfully created ${ChatColor.YELLOW}Checkpoint #${index + 1}${ChatColor.GREEN}!")
    }
}