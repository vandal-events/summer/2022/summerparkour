package events.vandal.summerparkour.commands.checkpoint

import dev.jorel.commandapi.executors.PlayerCommandExecutor
import events.vandal.summerparkour.SummerParkour
import events.vandal.summerparkour.parkour.ParkourManager
import org.bukkit.ChatColor
import org.bukkit.entity.Player

object ParkourCheckpointRemoveCommand : PlayerCommandExecutor {
    override fun run(sender: Player, args: Array<out Any>) {
        val parkour = ParkourManager.getSelectedParkourOrReport(sender) ?: return

        val index = (args[0] as Int) - 1

        if (index < 0 || index > parkour.checkpoints.size) {
            sender.sendMessage("${ChatColor.RED}Index out of range!")
            return
        }

        SummerParkour.plugin.server.getEntity(parkour.checkpoints[index].entityUuid)?.remove()
        parkour.checkpoints.removeAt(index)
        sender.sendMessage("${ChatColor.GREEN}Removed ${ChatColor.YELLOW}Checkpoint #${index + 1}${ChatColor.GREEN}!")
    }
}