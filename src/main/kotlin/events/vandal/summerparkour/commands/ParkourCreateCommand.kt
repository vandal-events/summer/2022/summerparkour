package events.vandal.summerparkour.commands

import dev.jorel.commandapi.executors.PlayerCommandExecutor
import events.vandal.summerparkour.SummerParkour
import events.vandal.summerparkour.parkour.Parkour
import events.vandal.summerparkour.parkour.ParkourManager
import org.bukkit.ChatColor
import org.bukkit.entity.Player
import java.util.*

object ParkourCreateCommand : PlayerCommandExecutor {
    override fun run(sender: Player, args: Array<out Any>) {
        val uuid = UUID.randomUUID()

        val parkour = Parkour(
            uuid,
            null,
            mutableListOf(),
            null,

            args[0] as String
        )

        ParkourManager.parkourMap[uuid] = parkour
        SummerParkour.selectedParkours[sender] = uuid

        ParkourManager.save()

        sender.sendMessage("${ChatColor.GREEN}Successfully created parkour ${ChatColor.YELLOW}\"${parkour.name}\"${ChatColor.GREEN}. UUID given is ${ChatColor.RED}$uuid")
    }
}