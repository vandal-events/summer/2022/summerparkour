package events.vandal.summerparkour.commands

import dev.jorel.commandapi.executors.PlayerCommandExecutor
import events.vandal.summerparkour.parkour.ParkourManager
import net.kyori.adventure.text.Component
import net.kyori.adventure.text.event.ClickEvent
import net.kyori.adventure.text.event.HoverEvent
import net.kyori.adventure.text.event.HoverEventSource
import net.kyori.adventure.text.format.NamedTextColor
import org.bukkit.ChatColor
import org.bukkit.entity.Player

object ParkourListCommand : PlayerCommandExecutor {
    override fun run(sender: Player, args: Array<out Any>) {
        sender.sendMessage("${ChatColor.GREEN}Parkours:")

        var count = 0
        ParkourManager.parkourMap.forEach {
            count++
            sender.sendMessage(
                Component.text("$count. ").color(NamedTextColor.GREEN).append(
                    Component.text("\"${it.value.name}\" / ").color(NamedTextColor.YELLOW).append(
                        Component.text("${it.value.uuid}").color(NamedTextColor.GOLD)
                    )
                        .clickEvent(ClickEvent.runCommand("/parkour select ${it.value.uuid}"))
                        .hoverEvent(HoverEvent.showText { Component.text("Click on me to select this parkour!") })
                )
            )
        }
    }
}