package events.vandal.summerparkour.commands

import dev.jorel.commandapi.executors.PlayerCommandExecutor
import events.vandal.summerparkour.SummerParkour
import events.vandal.summerparkour.parkour.ParkourCheckpoint
import events.vandal.summerparkour.parkour.ParkourManager
import net.kyori.adventure.text.Component
import net.kyori.adventure.text.format.NamedTextColor
import net.kyori.adventure.text.format.TextDecoration
import org.bukkit.ChatColor
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.entity.ArmorStand
import org.bukkit.entity.EntityType
import org.bukkit.entity.Player
import org.bukkit.event.entity.CreatureSpawnEvent

object ParkourSetStartCommand : PlayerCommandExecutor {
    override fun run(sender: Player, args: Array<out Any>) {
        if (SummerParkour.filterParkours(sender.location).isNotEmpty()) {
            sender.sendMessage("${ChatColor.RED}Position has already been taken by another point!")
            return
        }

        val parkour = ParkourManager.getSelectedParkourOrReport(sender) ?: return

        // Remove original stuff.
        val location = sender.location
        parkour.startPosition?.location?.world?.getEntity(parkour.startPosition!!.entityUuid)?.remove()
        parkour.startPosition?.location?.block?.type = Material.AIR

        val entity = location.world.spawnEntity(Location(location.world, location.blockX + 0.5, location.blockY + 0.5, location.blockZ + 0.5), EntityType.ARMOR_STAND, CreatureSpawnEvent.SpawnReason.CUSTOM) {
            val armorStand = it as ArmorStand
            armorStand.isSmall = true
            armorStand.isVisible = false
            armorStand.isInvisible = true
            armorStand.isInvulnerable = false
            armorStand.customName(Component.text("Parkour Start").color(NamedTextColor.GREEN).decorate(TextDecoration.BOLD))
            armorStand.isCustomNameVisible = true
            armorStand.setGravity(false)
        }

        location.block.type = Material.LIGHT_WEIGHTED_PRESSURE_PLATE

        parkour.startPosition = ParkourCheckpoint(
            location.toBlockLocation(),
            entity.uniqueId
        )

        ParkourManager.save()

        sender.sendMessage("${ChatColor.GREEN}Successfully created starting point at ${ChatColor.YELLOW}${location.blockX} ${location.blockY} ${location.blockZ}${ChatColor.GREEN}.")
    }
}