package events.vandal.summerparkour.commands

import dev.jorel.commandapi.executors.PlayerCommandExecutor
import events.vandal.summerparkour.SummerParkour
import events.vandal.summerparkour.parkour.ParkourManager
import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.entity.Player

object ParkourDeleteCommand : PlayerCommandExecutor {
    override fun run(sender: Player, args: Array<out Any>) {
        val parkour = ParkourManager.getSelectedParkourOrReport(sender) ?: return

        parkour.checkpoints.forEach {
            it.location.world.getEntity(it.entityUuid)?.remove()
            it.location.block.type = Material.AIR
        }

        parkour.startPosition?.location?.world?.getEntity(parkour.startPosition!!.entityUuid)?.remove()
        parkour.endPosition?.location?.world?.getEntity(parkour.endPosition!!.entityUuid)?.remove()

        parkour.startPosition?.location?.block?.type = Material.AIR
        parkour.endPosition?.location?.block?.type = Material.AIR

        ParkourManager.parkourMap.remove(parkour.uuid)
        SummerParkour.selectedParkours.filter { it.value == parkour.uuid }.toMutableMap().forEach {
            SummerParkour.selectedParkours.remove(it.key)

            it.key.sendMessage("${ChatColor.RED}The parkour you had selected earlier, ${ChatColor.YELLOW}\"${parkour.name}\"${ChatColor.RED}, has been deleted, so your parkour selection has been reset.")
        }

        ParkourManager.save()

        sender.sendMessage("${ChatColor.GREEN}Successfully deleted parkour!")
    }
}