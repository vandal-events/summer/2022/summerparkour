package events.vandal.summerparkour.parkour

import org.bukkit.Location
import org.bukkit.inventory.ItemStack
import java.util.*

data class ParkourPlayer(
    val startTime: Long,
    val collectedCheckpoints: MutableList<Pair<Long, Location>>,
    val currentParkour: UUID,
    val originalInventory: List<ItemStack?>?
)