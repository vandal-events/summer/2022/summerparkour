package events.vandal.summerparkour.parkour

import com.google.gson.*
import events.vandal.summerparkour.SummerParkour
import net.kyori.adventure.text.Component
import net.kyori.adventure.text.format.NamedTextColor
import net.kyori.adventure.text.format.TextColor
import net.kyori.adventure.text.format.TextDecoration
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.event.inventory.InventoryType
import org.bukkit.inventory.ItemStack
import java.io.File
import java.util.*

object ParkourManager {
    val parkourMap = mutableMapOf<UUID, Parkour>()
    private val parkourFile = File(SummerParkour.plugin.dataFolder, "parkour.json")

    val players = mutableMapOf<UUID, ParkourPlayer>()

    val customInventory = Bukkit.createInventory(null, InventoryType.PLAYER).apply {
        this.setItem(3, ItemStack(Material.HEAVY_WEIGHTED_PRESSURE_PLATE, 1).apply {
            val meta = this.itemMeta
            meta.displayName(Component.text("Teleport to Checkpoint").color(TextColor.color(0xD8B779)).decorate(TextDecoration.BOLD))

            this.itemMeta = meta
        })

        this.setItem(4, ItemStack(Material.LIGHT_WEIGHTED_PRESSURE_PLATE, 1).apply {
            val meta = this.itemMeta
            meta.displayName(Component.text("Reset to Start").color(NamedTextColor.GOLD).decorate(TextDecoration.BOLD))

            this.itemMeta = meta
        })

        this.setItem(5, ItemStack(Material.BARRIER, 1).apply {
            val meta = this.itemMeta
            meta.displayName(Component.text("Cancel").color(NamedTextColor.RED).decorate(TextDecoration.BOLD))

            this.itemMeta = meta
        })
    }

    fun loadAll() {
        if (!parkourFile.exists())
            return

        val json = JsonParser.parseString(parkourFile.readText()).asJsonObject

        val parkourList = json.getAsJsonObject("parkours")
        parkourList.keySet().forEach {
            val serializedParkourData = parkourList.getAsJsonObject(it)

            val uuid = UUID.fromString(it)

            val parkour = Parkour(
                name = serializedParkourData.get("name").asString,
                startPosition = ParkourCheckpoint(
                    SummerParkour.deserializePosition(serializedParkourData.get("startPosition").asJsonObject.get("location").asString) ?: return@forEach,
                    UUID.fromString(serializedParkourData.get("startPosition").asJsonObject.get("entityUuid").asString)
                ),
                checkpoints = mutableListOf<ParkourCheckpoint>().apply {
                    if (!serializedParkourData.has("checkpoints"))
                        return@apply

                    serializedParkourData.get("checkpoints").asJsonArray.forEach checkpoints@{ checkpoint ->
                        val checkpointData = checkpoint.asJsonObject

                        this.add(ParkourCheckpoint(
                            location = SummerParkour.deserializePosition(checkpointData.get("location").asString) ?: return@checkpoints,
                            entityUuid = UUID.fromString(checkpointData.get("entityUuid").asString)
                        ))
                    }
                },
                endPosition = ParkourCheckpoint(
                    SummerParkour.deserializePosition(serializedParkourData.get("endPosition").asJsonObject.get("location").asString) ?: return@forEach,
                    UUID.fromString(serializedParkourData.get("endPosition").asJsonObject.get("entityUuid").asString)
                ),

                uuid = uuid
            )

            parkourMap[uuid] = parkour
        }
    }

    fun save() {
        val json = JsonObject()

        val parkours = JsonObject()

        parkourMap.forEach { (uuid, parkour) ->
            if (parkour.startPosition == null || parkour.endPosition == null) {
                SummerParkour.plugin.slF4JLogger.warn("Parkour $uuid / \"${parkour.name}\" ")

                return@forEach
            }

            val serializedParkour = JsonObject()
            serializedParkour.add("startPosition", JsonObject().apply {
                this.addProperty("location", SummerParkour.serializePosition(parkour.startPosition!!.location))
                this.addProperty("entityUuid", parkour.startPosition!!.entityUuid.toString())
            })

            val checkpoints = JsonArray()
            parkour.checkpoints.forEach {
                checkpoints.add(JsonObject().apply {
                    this.addProperty("location", SummerParkour.serializePosition(it.location))
                    this.addProperty("entityUuid", it.entityUuid.toString())
                })
            }
            serializedParkour.add("checkpoints", checkpoints)

            serializedParkour.add("endPosition", JsonObject().apply {
                this.addProperty("location", SummerParkour.serializePosition(parkour.endPosition!!.location))
                this.addProperty("entityUuid", parkour.endPosition!!.entityUuid.toString())
            })
            serializedParkour.addProperty("name", parkour.name)

            parkours.add(uuid.toString(), serializedParkour)
        }

        json.add("parkours", parkours)

        parkourFile.writeText(GsonBuilder().apply {
            this.setPrettyPrinting()
        }.create().toJson(json))
    }

    fun getSelectedParkourOrReport(player: Player): Parkour? {
        val selectedParkourUUID = SummerParkour.selectedParkours[player]

        if (selectedParkourUUID == null) {
            player.sendMessage("${ChatColor.RED}You don't have a parkour selected!")
            return null
        }

        val selectedParkour = parkourMap[selectedParkourUUID]

        if (selectedParkour == null) {
            player.sendMessage("${ChatColor.RED}The parkour you have selected does not exist!")
            return null
        }

        return selectedParkour
    }
}