package events.vandal.summerparkour.parkour

import org.bukkit.Location
import java.util.*

data class ParkourCheckpoint(
    val location: Location,
    var entityUuid: UUID
)
