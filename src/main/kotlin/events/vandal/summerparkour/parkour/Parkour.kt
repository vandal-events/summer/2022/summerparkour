package events.vandal.summerparkour.parkour

import org.bukkit.Location
import java.util.*

data class Parkour(
    val uuid: UUID,

    var startPosition: ParkourCheckpoint?,
    val checkpoints: MutableList<ParkourCheckpoint>,
    var endPosition: ParkourCheckpoint?,

    var name: String
)
