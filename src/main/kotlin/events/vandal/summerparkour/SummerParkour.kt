package events.vandal.summerparkour

import dev.jorel.commandapi.*
import dev.jorel.commandapi.arguments.ArgumentSuggestions
import dev.jorel.commandapi.arguments.GreedyStringArgument
import dev.jorel.commandapi.arguments.IntegerArgument
import dev.jorel.commandapi.arguments.StringArgument
import events.vandal.metrics.MetricManager
import events.vandal.summerparkour.commands.*
import events.vandal.summerparkour.commands.checkpoint.ParkourCheckpointAddCommand
import events.vandal.summerparkour.commands.checkpoint.ParkourCheckpointAddIndexedCommand
import events.vandal.summerparkour.commands.checkpoint.ParkourCheckpointRemoveCommand
import events.vandal.summerparkour.commands.select.ParkourDeselectCommand
import events.vandal.summerparkour.commands.select.ParkourSelectBasedOnPlateCommand
import events.vandal.summerparkour.commands.select.ParkourSelectCommand
import events.vandal.summerparkour.commands.select.ParkourSelectedCommand
import events.vandal.summerparkour.events.ParkourEvent
import events.vandal.summerparkour.events.PlayerInventoryEvent
import events.vandal.summerparkour.metrics.ParkourMetricData
import events.vandal.summerparkour.parkour.Parkour
import events.vandal.summerparkour.parkour.ParkourManager
import org.bukkit.Location
import org.bukkit.NamespacedKey
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin
import java.util.*

class SummerParkour : JavaPlugin() {
    override fun onLoad() {
        CommandAPI.onLoad(CommandAPIConfig())
    }

    override fun onEnable() {
        CommandAPI.onEnable(this)

        CommandAPICommand("parkour").apply {
            withPermission("summerparkour.admin")
            withRequirement { it is Player }

            // /parkour select
            withSubcommand(
                CommandAPICommand("select").apply {
                    executesPlayer(ParkourSelectBasedOnPlateCommand)
                }
            )

            // /parkour select ec70bcaf-702f-4bb8-b48d-276fa52a780c
            withSubcommand(
                CommandAPICommand("select").apply {
                    withArguments(
                        StringArgument("uuid").replaceSuggestions(
                            ArgumentSuggestions.stringsWithTooltips {
                                val list = mutableListOf<IStringTooltip>()

                                ParkourManager.parkourMap.forEach { (uuid, parkour) ->
                                    list.add(StringTooltip.of(uuid.toString(), "Name : ${parkour.name}"))
                                }

                                list.toTypedArray()
                            }
                        )
                    )

                    executesPlayer(ParkourSelectCommand)
                }
            )

            withSubcommand(
                CommandAPICommand("checkpoint").apply {
                    // /parkour checkpoint add
                    withSubcommand(
                        CommandAPICommand("add").apply {
                            executesPlayer(ParkourCheckpointAddCommand)
                        }
                    )

                    // /parkour checkpoint add 3
                    withSubcommand(
                        CommandAPICommand("add").apply {
                            withArguments(IntegerArgument("index"))

                            executesPlayer(ParkourCheckpointAddIndexedCommand)
                        }
                    )

                    // /parkour checkpoint remove 2
                    withSubcommand(
                        CommandAPICommand("remove").apply {
                            withArguments(IntegerArgument("index"))

                            executesPlayer(ParkourCheckpointRemoveCommand)
                        }
                    )

                    // /parkour checkpoint swap 3 1
                    // /parkour checkpoint swap {current} {new}
                    /*withSubcommand(
                        CommandAPICommand("swap").apply {
                            withArguments(IntegerArgument("current_index"), IntegerArgument("new_index"))

                            executesPlayer(ParkourCheckpointSwapCommand)
                        }
                    )*/
                }
            )

            // /parkour set-start
            withSubcommand(
                CommandAPICommand("set-start").apply {
                    executesPlayer(ParkourSetStartCommand)
                }
            )

            // /parkour set-end
            withSubcommand(
                CommandAPICommand("set-end").apply {
                    executesPlayer(ParkourSetEndCommand)
                }
            )

            // /parkour list
            withSubcommand(
                CommandAPICommand("list").apply {
                    executesPlayer(ParkourListCommand)
                }
            )

            // /parkour selected
            withSubcommand(
                CommandAPICommand("selected").apply {
                    executesPlayer(ParkourSelectedCommand)
                }
            )

            // /parkour rename well this is interesting
            withSubcommand(
                CommandAPICommand("rename").apply {
                    withArguments(GreedyStringArgument("new_name"))
                    executesPlayer(ParkourRenameCommand)
                }
            )

            // /parkour create well this is interesting
            withSubcommand(
                CommandAPICommand("create").apply {
                    withArguments(GreedyStringArgument("name"))
                    executesPlayer(ParkourCreateCommand)
                }
            )

            withSubcommand(
                CommandAPICommand("delete").apply {
                    withAliases("remove")
                    executesPlayer(ParkourDeleteCommand)
                }
            )

            withSubcommand(
                CommandAPICommand("info").apply {
                    executesPlayer(ParkourInfoCommand)
                }
            )

            withSubcommand(
                CommandAPICommand("deselect").apply {
                    executesPlayer(ParkourDeselectCommand)
                }
            )
        }.register()

        plugin = this
        metrics = MetricManager(ParkourMetricData::class, this.dataFolder.apply { this.mkdirs() }.toPath(), "Summer 2022")

        ParkourManager.loadAll()

        this.server.scheduler.runTaskTimer(this, Runnable {
            ParkourManager.save()

            if (PlayerInventoryEvent.customInventories.any { !it.key.isOnline }) {
                PlayerInventoryEvent.customInventories.filter { !it.key.isOnline }.toMap().forEach {
                    PlayerInventoryEvent.customInventories.remove(it.key)
                }
            }
        }, 0L, 18_000L)

        this.server.pluginManager.registerEvents(ParkourEvent, this)
        this.server.pluginManager.registerEvents(PlayerInventoryEvent, this)
    }

    override fun onDisable() {
        CommandAPI.unregister("parkour")

        metrics.save()
        ParkourManager.save()
    }

    companion object {
        lateinit var plugin: JavaPlugin
        lateinit var metrics: MetricManager<ParkourMetricData>

        val selectedParkours = mutableMapOf<Player, UUID>()

        fun serializePosition(loc: Location): String {
            return "${loc.world.key.asString()};${loc.blockX};${loc.blockY};${loc.blockZ};${loc.yaw};${loc.pitch}"
        }

        fun deserializePosition(loc: String): Location? {
            val splitLoc = loc.split(";")

            val world = try {
                this.plugin.server.getWorld(NamespacedKey.fromString(splitLoc[0])!!)
            } catch (e: Exception) {
                this.plugin.slF4JLogger.error("Failed to load location $loc!")
                e.printStackTrace()

                null
            } ?: return null

            return Location(
                world,
                splitLoc[1].toDouble() + 0.5,
                splitLoc[2].toDouble() + 0.5,
                splitLoc[3].toDouble() + 0.5,
                splitLoc[4].toFloat(), splitLoc[5].toFloat()
            )
        }

        fun filterParkours(loc: Location): Map<UUID, Parkour> {
            return ParkourManager.parkourMap.filter { it.value.startPosition?.location?.block == loc.block || it.value.endPosition?.location?.block == loc.block || it.value.checkpoints.any { checkpoint -> checkpoint.location.block == loc.block } }
        }
    }
}