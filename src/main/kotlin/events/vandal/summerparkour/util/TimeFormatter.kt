package events.vandal.summerparkour.util

object TimeFormatter {
    private fun pad2(num: Int): String {
        return if (num < 0)
            "00"
        else if (num > 9)
            num.toString()
        else
            "0$num"
    }

    private fun pad3(num: Int): String {
        return if (num < 0)
            "000"
        else if (num > 99)
            num.toString()
        else if (num > 9)
            "0$num"
        else
            "00$num"
    }

    fun formatString(time: Long): String {
        //val days = pad2((time / (1000 * 60 * 60 * 24)).toInt())
        val hours = pad2(((time % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)).toInt())
        val minutes = pad2(((time % (1000 * 60 * 60)) / (1000 * 60)).toInt())
        val seconds = pad2(((time % (1000 * 60)) / 1000).toInt())
        val milliseconds = pad3((time % 1000).toInt())

        return "$hours:$minutes:$seconds.$milliseconds"
    }
}