package events.vandal.summerparkour.metrics

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.mojang.authlib.GameProfile
import events.vandal.metrics.MetricData
import events.vandal.summerparkour.SummerParkour
import events.vandal.summerparkour.parkour.ParkourPlayer
import org.bukkit.Location
import java.util.*

data class ParkourMetricData(
    val player: GameProfile,
    val parkourInfos: MutableList<ParkourInfoMetric> = mutableListOf(),
    val totalAttempts: MutableMap<UUID, Int> = mutableMapOf() // UUID is the Parkour UUID
) : MetricData(player) {
    override fun serialize(): JsonObject {
        return super.serialize().apply {
            val times = JsonArray()

            parkourInfos.forEach {
                val finishTimes = JsonObject()

                finishTimes.addProperty("startTime", it.startTime)
                finishTimes.addProperty("endTime", it.endTime)
                finishTimes.addProperty("parkourUuid", it.parkourUuid.toString())

                val parkourCheckpoints = JsonArray()

                it.collectedCheckpoints.forEach { checkpoint ->
                    val data = JsonObject()
                    data.addProperty("location", SummerParkour.serializePosition(checkpoint.key))
                    data.addProperty("time", checkpoint.value)

                    parkourCheckpoints.add(data)
                }

                finishTimes.add("parkourCheckpoints", parkourCheckpoints)

                times.add(finishTimes)
            }

            this.add("parkourInfos", times)

            val totalAttempts = JsonObject()
            this@ParkourMetricData.totalAttempts.forEach {
                totalAttempts.addProperty(it.key.toString(), it.value)
            }

            this.add("totalAttempts", totalAttempts)
        }
    }

    fun addParkourPlayer(parkour: ParkourPlayer, finishTime: Long) {
        val metric = ParkourInfoMetric(
            parkour.startTime,
            finishTime,
            mutableMapOf<Location, Long>().apply {
                parkour.collectedCheckpoints.forEach {
                    this[it.second] = it.first
                }
            },
            parkour.currentParkour
        )

        parkourInfos.add(metric)
    }

    companion object {
        @Suppress("unused")
        fun deserialize(data: JsonObject): ParkourMetricData {
            val profile = MetricData.deserialize(data)

            return ParkourMetricData(
                profile,
                mutableListOf<ParkourInfoMetric>().apply {
                    val array = data.getAsJsonArray("parkourInfos")

                    array.forEach {
                        val infos = it.asJsonObject
                        this.add(
                            ParkourInfoMetric(
                                infos.get("startTime").asLong,
                                infos.get("endTime").asLong,
                                mutableMapOf<Location, Long>().apply {
                                    val collectedCheckpoints = infos.getAsJsonArray("parkourCheckpoints")

                                    collectedCheckpoints.forEach { checkpoint ->
                                        val json = checkpoint.asJsonObject

                                        this[SummerParkour.deserializePosition(json.get("location").asString)!!] = json.get("time").asLong
                                    }
                                },
                                UUID.fromString(infos.get("parkourUuid").asString)
                            )
                        )
                    }
                },
                mutableMapOf<UUID, Int>().apply {
                    val attempts = data.getAsJsonObject("totalAttempts")
                    attempts.keySet().forEach {
                        this[UUID.fromString(it)] = attempts.get(it).asInt
                    }
                }
            )
        }
    }
}