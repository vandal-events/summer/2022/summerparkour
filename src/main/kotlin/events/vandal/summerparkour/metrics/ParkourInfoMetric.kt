package events.vandal.summerparkour.metrics

import org.bukkit.Location
import java.util.*

data class ParkourInfoMetric(
    val startTime: Long,
    val endTime: Long,
    val collectedCheckpoints: Map<Location, Long>,
    val parkourUuid: UUID
)
